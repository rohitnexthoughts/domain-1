package helloWorld

class User {
    String username
    String age
    static constraints = {
        username(nullable: false)
        age(min: false)
    }
}
