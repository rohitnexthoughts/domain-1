

dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    dialect = 'org.hibernate.dialect.MySQL5InnoDBDialect'
}

environments {
    development {
        dataSource {
            dbCreate = "create"
            url = "jdbc:mysql://localhost:3306/domains_demo?autoreconnect=true"
            username = "root"
            logSql = false
            password = "nextdefault"
        }
    }
    test {


        dataSource {
            dbCreate = "create"
            url = "jdbc:mysql://localhost:3306/domains_demo?autoreconnect=true"
            username = "root"
            logSql = false
            password = "nextdefault"
        }
    }
    production {
        dataSource {
            username = "root"
            password = "nextdefault"
            dbCreate = "create"
            url = "jdbc:mysql://localhost:3306/domains_demo?autoreconnect=true"
            pooled = true
            properties {
                maxActive = -1
                minEvictableIdleTimeMillis = 1800000
                timeBetweenEvictionRunsMillis = 1800000
                numTestsPerEvictionRun = 3
                testOnBorrow = true
                testWhileIdle = true
                testOnReturn = true
                validationQuery = "SELECT 1"
            }
        }
    }
}
grails.plugin.springsecurity.debug.useFilter = true


// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        [pattern: '/', access: ['permitAll']],
        [pattern: '/error', access: ['permitAll']],
        [pattern: '/index', access: ['permitAll']],
        [pattern: '/index.gsp', access: ['permitAll']],
        [pattern: '/shutdown', access: ['permitAll']],
        [pattern: '/assets/**', access: ['permitAll']],
        [pattern: '/j_spring_security_check', access: ['permitAll']],

//        [pattern: '/partials/**', access: ['permitAll']],
//        [pattern: '/api/**', access: ['permitAll']],
        [pattern: '/**/js/**', access: ['permitAll']],
        [pattern: '/**/css/**', access: ['permitAll']],
        [pattern: '/**/images/**', access: ['permitAll']],
        [pattern: '/**/favicon.ico', access: ['permitAll']],
        [pattern: '/**/**/img/**', access: ['permitAll']],
        [pattern: '/**/**/img/login-img.jpg', access: ['permitAll']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
        [pattern: '/assets/**', filters: 'none'],
        [pattern: '/**/js/**', filters: 'none'],
        [pattern: '/**/css/**', filters: 'none'],
        [pattern: '/**/images/**', filters: 'none'],
        [pattern: '/**/favicon.ico', filters: 'none'],
        [pattern: '/**', filters: 'JOINED_FILTERS']
        //         ,-restTokenValidationFilter,-restExceptionTranslationFilter,-restLogoutFilter']
//        [pattern: '/api/**', filters: 'JOINED_FILTERS,-anonymousAuthenticationFilter,-exceptionTranslationFilter,-authenticationProcessingFilter,' +
//                '-securityContextPersistenceFilter,-rememberMeAuthenticationFilter,-restLogoutFilter']
]


localizations.cache.size.kb = 1024





grails.gorm.default.constraints = {
    myCustomScale(scale: 10, max: 9999999999999999999999.99999999)
}

grails.mail.protocol = 'aws'
beans {
    mailSender.host = 'email-smtp.us-east-1.amazonaws.com'
}

// Added by the Audit-Logging plugin:
grails {
    plugin {
        auditLog {
            auditDomainClassName = 'com.crowdlending.audit.AuditTrail'
            logIds = true  // log db-ids of associated objects.
            actorClosure = { request, session ->
                if (request.applicationContext.springSecurityService.principal instanceof String) {
                    return request.applicationContext.springSecurityService.principal
                }
                def username = request.applicationContext.springSecurityService.principal?.username
                if (SpringSecurityUtils.isSwitched()) {
                    username = SpringSecurityUtils.switchedUserOriginalUsername + " AS " + username
                }
                return username
            }
        }
    }
}



grails.mime.types = [html         : ['text/html', 'application/xhtml+xml'],
                     xml          : ['text/xml', 'application/xml'],
                     text         : 'text-plain',
                     js           : 'text/javascript',
                     rss          : 'application/rss+xml',
                     atom         : 'application/atom+xml',
                     css          : 'text/css',
                     csv          : 'text/csv',
                     pdf          : 'application/pdf',
                     rtf          : 'application/rtf',
                     excel        : 'application/vnd.ms-excel',
                     ods          : 'application/vnd.oasis.opendocument.spreadsheet',
                     all          : '*/*',
                     json         : ['application/json', 'text/json'],
                     form         : 'application/x-www-form-urlencoded',
                     multipartForm: 'multipart/form-data'
]
