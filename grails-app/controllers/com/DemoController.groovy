package com

class DemoController {
    static scaffold = Author

    def index() {
//        Author author = new Author(name: 'a1').save(flush: true)
//        Book book = new Book(name: 'b1').save(flush: true)
//        author.addToBooks(book).save(flush: true)
//        Book book2 = new Book(name: 'b1').save(flush: true)
//        author.addToBooks(book2).save(flush: true)

        Author author = new Author(name: 'a1').save(flush: true)
        Book book = new Book(name: 'b1', author: author).save(flush: true)
        author.addToBooks(book).save(flush: true)
        Book book2 = new Book(name: 'b2', author: author).save(flush: true)
        author.addToBooks(book2).save(flush: true)
//
//
//        Author author = new Author(name: 'a1').save(flush: true)
//        Book book = new Book(name: 'b1', author: author).save(flush: true)
//        Book book2 = new Book(name: 'b2', author: author).save(flush: true)
//        println "author   " + author.books
        render author.books ?: 'null'
    }
}
