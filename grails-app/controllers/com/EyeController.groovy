package com

class EyeController {

    def index() {
        Face face = null
        Eye eye = null
        Face.withSession { session ->
            face = new Face().save(flush: true)
            eye = new Eye(face: face).save(flush: true)
            session.flush()
        }

//        Thread.sleep(30000);
        Face.withNewSession {
            println "face -> eyes" + Face.get(1).eyes
        }


        render "Success "
    }
}
