package com

class BookController {

    def index() {
        Author author = new Author()
        author.name = "A1"
        Book book = new Book()
        book.name = "Book1"
        book.author = author
        author.addToBooks(book)
        author.save(flush: true)
        render "ok"
    }


}
