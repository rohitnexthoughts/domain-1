package com

class StudentController {

    def index() {
        College college = new College()
        Student student = new Student()
        student.college = college
        college.addToStudents(student)
        college.save(flush: true)

        println "college students  " + college.students
        println "student -> college " + student.college
        render "ok"
    }
}
