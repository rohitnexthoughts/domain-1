package com

class NoseController {

    def index() {

        Face face = new Face()
        Nose nose = new Nose()
        nose.face = face
        face.nose = nose
        face.save(flush: true)

        println "=====face-> nose " + face.nose
        println "=====nose-> face " + nose.face


        render "ok"
    }
}
